#include <QApplication>
#include <QMessageBox>
#include <QDebug>

#include <QWidget>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>
#include <QIcon>
#include <QSize>
#include <QPixmap>

#include "QTcpClientSysTray.h"
#include "QTitlePanel.h"
#include "config.h"

QTcpClientSysTray::QTcpClientSysTray()
{
    QIcon icon = QIcon(Q_ICON_TRAY_PATH);
    trayIcon_ = new QSystemTrayIcon(this);
 //   trayIcon_->setIcon(icon);
    trayIcon_->setIcon(QPixmap(Q_ICON_TRAY_PATH));
    trayIcon_->setToolTip(QObject::tr("TcpClient"));
    QString title = QObject::tr("title");
    QString text = QObject::tr("this is a test");
    trayIcon_->show();

    //弹出气泡提示
    trayIcon_->showMessage(title, text, QSystemTrayIcon::Information, 50000);

    //添加单击/双击鼠标
    connect(trayIcon_, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(slot_trayIconActivated(QSystemTrayIcon::ActivationReason)));

    //创建监听行为
    minimizeAction_ = new QAction(QObject::tr("minimize (&I)"), this);
    restoreAction_  = new QAction(QObject::tr("restore (&R)"), this);
    quitAction_     = new QAction(QObject::tr("quit (&Q)"), this);

    connect(minimizeAction_, SIGNAL(triggered()), this, SLOT(hide()));
    connect(restoreAction_, SIGNAL(triggered()), this, SLOT(showNormal()));
//    connect(quitAction_, SIGNAL(triggered()), qApp, SLOT(quit()));

    //创建右键弹出菜单
    trayIconMenu_ = new QMenu(this);
    trayIconMenu_->addAction(minimizeAction_);
    trayIconMenu_->addAction(restoreAction_);
    trayIconMenu_->addSeparator();
    trayIconMenu_->addAction(quitAction_);
    trayIcon_->setContextMenu(trayIconMenu_);
}


QTcpClientSysTray::QTcpClientSysTray(QWidget *parent)
    :QWidget(parent)
{
    QIcon icon = QIcon(Q_ICON_TRAY_PATH);
    trayIcon_ = new QSystemTrayIcon(this);
    trayIcon_->setIcon(icon);
    trayIcon_->setToolTip(QObject::tr("TcpClient"));
    QString title = QObject::tr("title");
    QString text = QObject::tr("this is a test");
    trayIcon_->show();

    //弹出气泡提示
    trayIcon_->showMessage(title, text, QSystemTrayIcon::Information, 1000);

    //添加单击/双击鼠标
    connect(trayIcon_, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(slot_trayIconActivated(QSystemTrayIcon::ActivationReason)));

    //创建监听行为
    minimizeAction_ = new QAction(QObject::tr("minimize (&I)"), this);
    restoreAction_  = new QAction(QObject::tr("restore (&R)"), this);
    quitAction_     = new QAction(QObject::tr("quit (&Q)"), this);

    connect(minimizeAction_, SIGNAL(triggered()), this, SLOT(hide()));
    connect(restoreAction_, SIGNAL(triggered()), this, SLOT(showNormal()));
//    connect(quitAction_, SIGNAL(triggered()), qApp, SLOT(quit()));

    //创建右键弹出菜单
    trayIconMenu_ = new QMenu(this);
    trayIconMenu_->addAction(minimizeAction_);
    trayIconMenu_->addAction(restoreAction_);
    trayIconMenu_->addSeparator();
    trayIconMenu_->addAction(quitAction_);
    trayIcon_->setContextMenu(trayIconMenu_);
}

QTcpClientSysTray::~QTcpClientSysTray()
{
    delete trayIcon_;
}

void QTcpClientSysTray::slot_trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch(reason)
    {
        case QSystemTrayIcon::Trigger:
        //    QMessageBox::information(this, "Trigger", "trayIcon Triggered!");
            qDebug()<<"slot_trayIconActivated Trigger \n";
            emit sgn_restore();
            break;
            //单击托盘图标
        case QSystemTrayIcon::DoubleClick:
            //双击托盘图标
  //          QMessageBox::information(this,"DoubleClick","trayIcon DoubleClicked!");
            qDebug()<<"slot_trayIconActivated DoubleClick\n";
            emit sgn_restore();
  //          this->showNormal();
  //          this->raise();
            break;
        case QSystemTrayIcon::MiddleClick:
            QMessageBox::information(this,"MiddleClick","trayIcon MiddleClicked!");
            break;
        case QSystemTrayIcon::Context:
        //    QMessageBox::information(this,"Context","trayIcon Context!");
            break;
        default:
            break;
    }
}

//int main(int argc, char *argv[])
//{
//    QApplication app(argc, argv);

//    QTcpClientSysTray sysTrayWin;

//    Qt::WindowFlags flags = 0;
//    flags |= Qt::FramelessWindowHint;
//    sysTrayWin.setWindowFlags(flags);

//    sysTrayWin.show();

//    QTitlePanel titlePanel;
//    titlePanel.show();

//    return app.exec();
//}
