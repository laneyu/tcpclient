#include <QLabel>
#include <QHBoxLayout>

#include <QDebug>

#include "QTitlePanel.h"
#include "QButton.h"
#include "config.h"

QTitlePanel::QTitlePanel(QWidget *parent)
    : QWidget(parent)
{
    titleLabel_  = new QLabel("title");
    minimizeBtn_ = new QButton;
    maximizeBtn_ = new QButton;
    quitBtn_     = new QButton;
    layout_      = new QHBoxLayout;

    minimizeBtn_->setText("min");
    maximizeBtn_->setText("max");
    quitBtn_->setText("quit");

    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    this->setWindowFlags(flags);

    NewActiveWidgets();

    connect(minimizeBtn_, SIGNAL(sgn_Clicked()), this, SLOT(slot_minimize()));
    connect(maximizeBtn_, SIGNAL(sgn_Clicked()), this, SLOT(slot_maxmize()));
    connect(quitBtn_, SIGNAL(sgn_Clicked()), this, SLOT(slot_quit()));
}

QTitlePanel::~QTitlePanel()
{
    delete titleLabel_;
    delete minimizeBtn_;
    delete maximizeBtn_;
    delete quitBtn_;
    delete layout_;
}

//public
void QTitlePanel::NewActiveWidgets()
{
    QSize btnSize = QSize(Q_TITLEPANEL_BTN_W, Q_TITLEPANEL_BTN_H);
    minimizeBtn_->setFixedSize(btnSize);
    maximizeBtn_->setFixedSize(btnSize);
    quitBtn_->setFixedSize(btnSize);
    titleLabel_->setFixedHeight(Q_TITLEPANEL_BTN_H);
    layout_->setContentsMargins(QMargins(0, 0, 0, 0));
    layout_->setSpacing(0);

    layout_->addWidget(titleLabel_);
    layout_->addWidget(minimizeBtn_);
 //   layout_->addWidget(maximizeBtn_);
    layout_->addWidget(quitBtn_);

    //背景颜色
    SetTitlePanelStyle();

    this->setFixedWidth(Q_TITLEPANEL_W);
    this->setLayout(layout_);
}

void QTitlePanel::SetTitlePanelStyle()
{
    //设置标题栏元素背景颜色
    titleLabel_->setStyleSheet(Q_TITLEPANEL_MEMBER_BACKCOCLOR);
    minimizeBtn_->setStyleSheet(Q_TITLEPANEL_MEMBER_BACKCOCLOR);
    maximizeBtn_->setStyleSheet(Q_TITLEPANEL_MEMBER_BACKCOCLOR);
    quitBtn_->setStyleSheet(Q_TITLEPANEL_MEMBER_BACKCOCLOR);


    //设置标题栏背景
    this->setStyleSheet(Q_TITLEPANEL_BACKCOCLOR);
}

//slots
void QTitlePanel::slot_maxmize()
{
    qDebug()<<"QTitlePanel slot_maxmize\n";
    emit sgn_maximize();
}

void QTitlePanel::slot_minimize()
{
    qDebug()<<"QTitlePanel slot_minimize\n";
    emit sgn_minimize();
}

void QTitlePanel::slot_quit()
{
    qDebug()<<"QTitlePanel slot_quit\n";

    emit sgn_quit();
}
