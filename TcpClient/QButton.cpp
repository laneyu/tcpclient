#include <QObject>
#include <QPushButton>
#include <QWidget>
#include <QEvent>
#include <QPaintEvent>
#include <QDebug>

#include "QButton.h"

QButton::QButton(QWidget *parent)
    :QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(slot_Clicked()));
}

//QButton::QButton(QString tips, QWidget *parent = 0)
//    :QPushButton(parent)
//{

//}

QButton::~QButton()
{

}

void QButton::enterEvent(QEvent *event)
{
    slot_Enter();
}

void QButton::leaveEvent(QEvent *event)
{
    slot_Leave();
}

//void QButton::paintEvent(QPaintEvent *event)
//{
//
//}

//slots
void QButton::slot_Enter()
{
    qDebug()<<"slot_Enter\n";
}

void QButton::slot_Leave()
{
    qDebug()<<"slot_Leave\n";
}

void QButton::slot_Clicked()
{
  qDebug()<<"slot_Clicked\n";
  emit sgn_Clicked();
}
