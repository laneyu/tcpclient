#ifndef QSHOWWIN_H
#define QSHOWWIN_H

#include <QObject>
#include <QWidget>
#include <QTextEdit>

class QShowWin : public QTextEdit
{
    Q_OBJECT

public:
    QShowWin(QWidget *parent = 0);

protected:
    void timerEvent(QTimerEvent *e);
};

class QSendWin : public QTextEdit
{
    Q_OBJECT

public:
    QSendWin(QWidget *parent = 0);

protected slots:
    void slot_Send();
};

#endif // QSHOWWIN_H
