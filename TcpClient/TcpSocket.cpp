#ifdef PLATFORM_LINUX
#include <errno.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netpacket/packet.h>
#include <arpa/inet.h>

#else

#endif


#include "TcpSocket.h"

int socket_;
char hostAddress_[20];
unsigned short hostPort_;
int hostStatus_;

#ifdef PLATFORM_LINUX
int SocketSendLen(int sockfd, const char *buf, int len)
{
    do
    {
        int ret = send(sockfd, buf, len, 0);

        if (ret <= 0)
        {
            perror("send to client error");
            return -1;
        }
        buf += ret;
        len -= ret;
    }
    while (len != 0);
    return 0;
}

int SocketRecv(int sockfd, char * buf, int len)
{
    int ret = 0;
    int read_len = 0;

    if (sockfd <= 0)
        return -1;

    while (read_len < len)
    {
        ret = recv(sockfd, buf, len - read_len, 0);

        if (ret <= 0)
        {
            return -1;
        }
        read_len += ret;
        buf += ret;
    }

    return len;
}

void setHostAddressAndPort(const char *address, unsigned short port)
{
    snprintf(hostAddress_, sizeof(hostAddress_), "%s", address);
    hostPort_ = port;
}


void connectionToServer(const char *address, unsigned short port)
{
    struct sockaddr_in server_addr;

    setHostAddressAndPort(address, port);

    socket_ = socket(AF_INET, SOCK_STREAM, 0);

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(hostAddress_);
    server_addr.sin_port = htons(hostPort_);

//    int flags;

//    flags = fcntl(socket_, F_GETFL, 0);
//    fcntl(socket_, F_SETFL, flags|O_NONBLOCK);

    int ireuseadd_on = 1;
    setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, &ireuseadd_on, sizeof(ireuseadd_on));
    int ret = connect(socket_, (struct sockaddr *)&server_addr, sizeof(struct sockaddr));
    if (ret == 0)
    {
    printf("connection ok!\n");
        hostStatus_ = 1;
    }
    else
    {
    printf("connection failed!\n");
        hostStatus_ = -1;
    }
}

int send(const char *buf, int len)
{
    if (hostStatus_ != 1)
    {
        return -1;
    }

    SocketSendLen(socket_, buf, len);

    return 0;
}

int recv(char *buf, int len)
{
    struct timeval tv;
    fd_set fds;
    int ret;

    if (hostStatus_ != 1)
    {
        return -1;
    }

    tv.tv_sec = 0;
    tv.tv_usec = 500000;

    FD_ZERO(&fds);
    FD_SET(socket_, &fds);

    ret = select(socket_+1, &fds, NULL, NULL, &tv);

    if (ret == 1)
        return SocketRecv(socket_, buf, len);
    else
        return 0;
}

int recv_one_frame()
{
    int bytes = 0;
    unsigned int length = 0;
    char len_str[4] = "0";
    char buf[1024] = "0";

    int ret = recv(len_str, 4);
    if (ret > 0)
    {
    printf("recv msg(%d):%s\n", ret,  len_str);

//	length = strtol(len_str, NULL, 10);
    length = atoi(len_str);
///    	length = (unsigned int)len_str[0]*0xff<<24 + (unsigned int)len_str[1]<<16 + (unsigned int)len_str[2]<<8 + (unsigned int)len_str[3];
    printf("frame length = %d\n", length);
    }
}



#if 0
int main()
{
    char buf[1024] = {0};

    connectionToServer("192.168.18.125", 50000);

//    while(1)
//    {
//       send("12345", strlen("12345\n")+1);
//       sleep(1);
//    }

   while(1)
   {
    recv_one_frame();
   }

    while(1)
    {
        int ret = recv(buf, 10);
        if (ret > 0)
            printf("recv msg(%d):%s\n", ret,  buf);
    }
}
#endif

#else
void connectionToServer(const char *address, unsigned short port)
{

}

int send(const char *buf, int len)
{
    return 0;
}

int recv(char *buf, int len)
{
    return 0;
}

#endif

