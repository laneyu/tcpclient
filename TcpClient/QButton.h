#ifndef QBUTTON_H
#define QBUTTON_H

#include <QObject>
#include <QPushButton>

class QString;
class QWidget;
class QEvent;
class QPaintEvent;

class QButton : public QPushButton
{
    Q_OBJECT
public:
    QButton(QWidget *parent = 0);
//    QButton(QString tips, QWidget *parent = 0);
    ~QButton();

protected:
    void enterEvent(QEvent* event);
    void leaveEvent(QEvent* event);
//    void paintEvent(QPaintEvent *event);

signals:
    void sgn_Clicked();

public slots:
    void slot_Enter();
    void slot_Leave();
    void slot_Clicked();
};

#endif // QBUTTON_H
