#include <QObject>
#include <QWidget>

#include "QTcpClient.h"

#include "QTitlePanel.h"

QTcpClient::QTcpClient()
{
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    this->setWindowFlags(flags);

    NewActiveWidgets();
}

void QTcpClient::NewActiveWidgets()
{
    showWin_ = new QShowWin();
}
