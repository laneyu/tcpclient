#ifndef QTCPCLIENT_H
#define QTCPCLIENT_H

#include <QObject>
#include <QWidget>
#include <QHBoxLayout>
#include "QTitlePanel.h"
#include "QShowWin.h"
#include "QButton.h"

class QTcpClient : public QWidget
{
    Q_OBJECT
public:
    QTcpClient();
    void NewActiveWidgets();

private:
    QHBoxLayout* layout_;
    QTitlePanel* titlePanel_;
    QShowWin*    showWin_;
    QButton*     btnSend_;
};

#endif // QTCPCLIENT_H
