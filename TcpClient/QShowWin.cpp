#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QDebug>

#include "QShowWin.h"
#include "config.h"
#include "TcpSocket.h"

QShowWin::QShowWin(QWidget *parent)
    :QTextEdit(parent)
{
    this->setReadOnly(true);

    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    this->setWindowFlags(flags);

    this->setFixedHeight(Q_SHOW_WIN_H);
    this->setFixedWidth(Q_SHOW_WIN_W);

    QObject::startTimer(100);
}

//定时刷新数据
void QShowWin::timerEvent(QTimerEvent *e)
{
    char buf[1024] = "0";

    int ret = recv(buf, sizeof(1024)-1);

    if (ret > 0)
    {
        this->append(buf);
    }
}

QSendWin::QSendWin(QWidget *parent)
    :QTextEdit(parent)
{
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    this->setWindowFlags(flags);

    this->setFixedHeight(Q_SEND_WIN_H);
    this->setFixedWidth(Q_SEND_WIN_W);
}

//数据发送
void QSendWin::slot_Send()
{
    qDebug() <<"slot_Send\n";
    qDebug() <<toPlainText().toAscii() <<"\n";
    send(toPlainText().toAscii(), strlen(toPlainText().toAscii()));
    
    this->clear();
}
