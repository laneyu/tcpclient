#ifndef QTCPCLIENTSYSTRAY_H
#define QTCPCLIENTSYSTRAY_H

#include <QObject>

#include <QWidget>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>

class QTcpClientSysTray : public QWidget
{
    Q_OBJECT
public:
    QTcpClientSysTray();
    QTcpClientSysTray(QWidget *parent);
    ~QTcpClientSysTray();

signals:
    void sgn_minimize();
    void sgn_restore();

public slots:
    void slot_trayIconActivated(QSystemTrayIcon::ActivationReason reason);

private:
    QSystemTrayIcon *trayIcon_;
    QMenu *trayIconMenu_;
    QAction *minimizeAction_;
    QAction *restoreAction_;
    QAction *quitAction_;
};

#endif // QTCPCLIENTSYSTRAY_H
