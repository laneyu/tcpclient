#ifndef CONFIG_H
#define CONFIG_H

//#define PLATFORM_LINUX

#define Q_TITLEPANEL_W         300
#define Q_TITLEPANEL_BTN_W     30
#define Q_TITLEPANEL_BTN_H     30
#define Q_TITLEPANEL_STYLE     "border-radius:5px; font-size: 17px;"
#define Q_TITLEPANEL_BACKCOCLOR "background-color: rgb(9, 69, 141);"
#define Q_TITLEPANEL_MEMBER_STYLE "border-radius:5px; font-size: 17px;"
#define Q_TITLEPANEL_MEMBER_BACKCOCLOR "color: rgb(255, 255, 255); background-color: rgb(0,69,250);" //成员颜色
#define Q_TCP_CLIENT_WIN_BACKCOLOR  "background-color:  rgb(180,180,180);"
#define Q_ICON_TRAY_PATH       ":/logo.bmp"
//#define Q_ICON_TRAY_PATH       "/home/button/exit.bmp"

#define Q_TCP_CLIENT_WIN_W     320
#define Q_TCP_CLIENT_WIN_H     500

#define Q_SHOW_WIN_W           300
#define Q_SHOW_WIN_H           250

#define Q_SEND_WIN_W           300
#define Q_SEND_WIN_H           100

#define Q_SEND_BTN_W           150
#define Q_SEND_BTN_H           30
#endif // CONFIG_H
