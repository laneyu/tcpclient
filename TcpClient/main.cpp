#include <QApplication>
#include <QVBoxLayout>
#include <QLabel>

#include "QTcpClientSysTray.h"
#include "QTitlePanel.h"
#include "QButton.h"
#include "QShowWin.h"
#include "config.h"
#include "QTcpClient.h"
#include "TcpSocket.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget *mainWin = new QWidget();
//    mainWin->setFixedHeight(Q_TCP_CLIENT_WIN_H);
//    mainWin->setFixedWidth(Q_TCP_CLIENT_WIN_W);
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    mainWin->setWindowFlags(flags);
    mainWin->setStyleSheet(Q_TCP_CLIENT_WIN_BACKCOLOR);

    QVBoxLayout *mainWinLayout = new QVBoxLayout;
    QTitlePanel *titlePanel_ = new QTitlePanel; //1
    QShowWin *showWin_ = new QShowWin;          //2
    QSendWin *sendWin_ = new QSendWin;          //3
    QButton  *sendBtn_ = new QButton;           //4
    QTcpClientSysTray *sysTray_ = new QTcpClientSysTray;

    //设置属性
    //4 sendBtn_
    sendBtn_->setText(QObject::tr("Send"));
    sendBtn_->setFixedSize(Q_SEND_BTN_W, Q_SEND_BTN_H);
    sendBtn_->setStyleSheet(Q_TITLEPANEL_MEMBER_BACKCOCLOR);



    mainWinLayout->addWidget(titlePanel_);
    mainWinLayout->addWidget(showWin_);
    mainWinLayout->addWidget(sendWin_);
    mainWinLayout->addWidget(sysTray_);
    mainWinLayout->addWidget(sendBtn_);
    // layout设置属性
    mainWinLayout->setAlignment(titlePanel_, Qt::AlignHCenter);
    mainWinLayout->setAlignment(showWin_, Qt::AlignHCenter);
    mainWinLayout->setAlignment(sendWin_, Qt::AlignHCenter);
    mainWinLayout->setAlignment(sendBtn_, Qt::AlignHCenter);
    mainWin->setLayout(mainWinLayout);

    QObject::connect(sysTray_, SIGNAL(sgn_minimize()), mainWin, SLOT(hide()));
    QObject::connect(sysTray_, SIGNAL(sgn_restore()), mainWin, SLOT(show()));   //托盘唤醒窗口

    QObject::connect(titlePanel_, SIGNAL(sgn_minimize()), mainWin, SLOT(hide()));//最小化
    QObject::connect(titlePanel_, SIGNAL(sgn_maximize()), mainWin, SLOT(show()));//最大化
    QObject::connect(titlePanel_, SIGNAL(sgn_quit()), &app, SLOT(quit()));  //退出窗口

    QObject::connect(sendBtn_, SIGNAL(clicked()), sendWin_, SLOT(slot_Send()));     //发送按钮

    //connect server
    connectionToServer("192.168.1.121", 50000);
    

    mainWin->show();

//    QTcpClientSysTray *sysTray = new QTcpClientSysTray;
//    sysTray->show();


//    QWidget tcpClient;

//    tcpClient.setFixedWidth(Q_TCP_CLIENT_WIN_W);
//    tcpClient.setFixedHeight(Q_TCP_CLIENT_WIN_H);

//    QTcpClientSysTray sysTray(&tcpClient);

//    tcpClient.show();



//    QTcpClientSysTray tcpClient;
//    tcpClient.show();

//    QTitlePanel titlePanel;
//    QObject::connect(&titlePanel, SIGNAL(sgn_quit()), &app, SLOT(quit()));
//    titlePanel.show();

//    QShowWin win;
//    win.setText("12345");
//    win.show();

    return app.exec();
}
