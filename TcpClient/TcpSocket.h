#ifndef TCPCLIENT_H
#define TCPCLIENT_H

void connectionToServer(const char *address, unsigned short port);
int send(const char *buf, int len);
int recv(char *buf, int len);

#endif // TCPCLIENT_H

