#ifndef QTITLEPANEL_H
#define QTITLEPANEL_H

#include <QObject>

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>

#include "QButton.h"

class QTitlePanel : public QWidget
{
    Q_OBJECT

public:
    QTitlePanel(QWidget *parent = 0);
    ~QTitlePanel();

    void NewActiveWidgets();
    void SetTitlePanelStyle();

signals:
    void sgn_minimize();
    void sgn_maximize();
    void sgn_quit();

public slots:
    void slot_minimize();
    void slot_maxmize();
    void slot_quit();

private:
    QLabel *titleLabel_;
    QButton *minimizeBtn_;
    QButton *maximizeBtn_;
    QButton *quitBtn_;
    QHBoxLayout *layout_;
};

#endif // QTITLEPANEL_H
